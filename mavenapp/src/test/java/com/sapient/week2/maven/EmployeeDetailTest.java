package com.sapient.week2.maven;

import static org.junit.jupiter.api.Assertions.*;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

class EmployeeDetailTest {

    private EmployeeDAO empDAO = new EmployeeDAO();
    private List<EmployeeBean> list = empDAO.readData();

    @Test
    public void readDataTest()
    {
        String s = "C:\\Users\\tejparma\\day2\\employeeDetails.csv";
        FileReader fileReader = null;
        try
        {
          fileReader = new FileReader(s);
        } catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        BufferedReader buffReader = new BufferedReader(fileReader);
        String line;
        List<EmployeeBean> correctlist = new ArrayList<EmployeeBean>();
        try
        {
            while((line=buffReader.readLine())!=null)
            {
                String data[] = line.split(",");
                int id = Integer.parseInt(data[0]);
                String name = data[1];
                int salary = Integer.parseInt(data[2]);
                EmployeeBean emp = new EmployeeBean(id,name,salary);
                correctlist.add(emp);
            }
        } catch (IOException e)
        {
            e.printStackTrace();
        }
    
        for(int i=0;i<list.size();i++)
        {
        	System.out.println(i);
            EmployeeBean emp1 = list.get(i);
            EmployeeBean emp2 = correctlist.get(i);
            assertEquals(emp1.equals(emp2), true);
        }
    }
    
    @Test
    public void getCountTest()
    {
        //System.out.println(empDAO.getCount(list, 57000));
        assertEquals(2, empDAO.getCount(list, 57000));
    }
    
    @Test
    public void getTotalSalaryTest()
    {
        assertEquals(976300, empDAO.getTotalSalary(list));
    }
    
    @Test
    public void getEmployeeTest()
    {
        assertEquals(true, empDAO.getEmployee(101).equals(new EmployeeBean(101,"Tejas",65000)));
    }
    
    

}