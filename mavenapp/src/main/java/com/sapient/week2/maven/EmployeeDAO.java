package com.sapient.week2.maven;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class EmployeeDAO {
	
	//List of employee to be populated
	List<EmployeeBean> list = new ArrayList<EmployeeBean>();
	
	//Buffered reader to read from the CSV file
	BufferedReader bufferedReader;
	
	//throws IO exception if bufferedReader fails
	public List<EmployeeBean> readData(){
		File file = new File("C:\\Users\\tejparma\\day2\\employeeDetails.csv");
		//FileReader throws FileNotFoundException
		try{
			bufferedReader = new BufferedReader(new FileReader(file));
		
		String line;
		EmployeeBean empBean;
		while((line=bufferedReader.readLine()) != null) {
			String data[] = line.split(",");
			//parse the given string elements to appropriate data types
			empBean = new EmployeeBean(Integer.parseInt(data[0]), data[1], Double.parseDouble(data[2]));
			//System.out.println(empBean.toString());
			list.add(empBean);
		}
		}
		catch(Exception e){
			
		}
		try {
			bufferedReader.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}
	
	public double getTotalSalary(List<EmployeeBean> empBeanList) {
		return empBeanList.stream().mapToDouble(employee -> employee.getSalary()).sum();
	}
	
	public long getCount(List<EmployeeBean> empBeanList, double salary) {
		return empBeanList.stream().filter(employee -> employee.getSalary() == salary).count();
	}
	
	public EmployeeBean getEmployee(int id) {
		List<EmployeeBean> tempList = list.stream().filter(employee -> employee.getId() == id).collect(Collectors.toList());
		return tempList.get(0);
	}
}
